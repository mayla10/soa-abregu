package org.soa.rest.spring.controller;

import java.util.List;

import org.soa.rest.spring.model.Card;
import org.soa.rest.spring.model.Card;
import org.soa.rest.spring.persist.CardDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

public class CardRestController {
	
	@Autowired
	private CardDAO cardDAO;

	
	@GetMapping("/cards")
	public List<Card> getCards() {
		return cardDAO.getAll();
	}

	@GetMapping("/cards/{id}")
	public ResponseEntity<Card> getCard(@PathVariable("id") Long id) {

		Card card = cardDAO.read(id);
		if (card == null) {
			return new ResponseEntity<Card>("No Card found for ID " + id, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Card>(card, HttpStatus.OK);
	}

	@PostMapping(value = "/cards")
	public ResponseEntity<Card> createCard(@RequestBody Card card) {

		cardDAO.add(card);

		return new ResponseEntity<Card>(card, HttpStatus.OK);
	}

	@DeleteMapping("/cards/{id}")
	public ResponseEntity<Long> deleteCard(@PathVariable Long id) {
		
		//Recupero card
		Card card = cardDAO.read(id);
		//Se la paso a remove
		cardDAO.remove(card);

		return new ResponseEntity<Long>(id, HttpStatus.OK);

	}

	@PutMapping("/cards/{id}")
	public ResponseEntity<Card> updateCard(@PathVariable Long id, @RequestBody Card card) {
		
		cardDAO.update(card);
		
		return new ResponseEntity<Card>(card, HttpStatus.OK);
	}

}
