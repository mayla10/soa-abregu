package org.soa.rest.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="CARD") 
public class Card extends GenericObject{

	@Column(name="ISSUER", length=30, nullable=false)
	private String cardIssuer;
	
	@Column(name="CODE", length=30, nullable=false)
	private String codeCard;
	
	
	public String getCardIssuer() {
		return cardIssuer;
	}
	
	public void setCardIssuer(String cardIssuer) {
		this.cardIssuer = cardIssuer;
	}
	
	public String getCodeCard() {
		return codeCard;
	}
	
	public void setCodeCard(String codeCard) {
		this.codeCard = codeCard;
	}


}
